﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/**
 * Author: Prince Nimoh
 * Student #: 000792122
 * Date: July 16, 2018
 * CPRG200 - Lab 4.
 * DB class for connecting to the Northwind database
 * */
namespace OrderData
{
    public class NorthwindDB
    { 
        /// <summary>
        /// Returns an SQL database connection for the NorthWind db
        /// </summary>
        /// <returns>SQL database connection</returns>
        public static SqlConnection GetSqlConnection()
        {
            string connectionstring = @"Data Source=localhost\sqlexpress;Initial Catalog=Northwind;Integrated Security=True";
            SqlConnection con = new SqlConnection(connectionstring);

            return con;
        }
    }
}
