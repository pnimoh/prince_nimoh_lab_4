﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderData
{
 /**
* Author: Prince Nimoh
* Student #: 000792122
* Date: July 16, 2018
* CPRG200 - Lab 4.
* Class containing fields for the OrderDetails class
* */
    public class OrderDetail
    {

        public int OrderID { get; set; }

        public int ProductID { get; set; }

        public decimal UnitPrice { get; set; }

        public int Quantity { get; set; }

        public double Discount { get; set; }
    }
}
