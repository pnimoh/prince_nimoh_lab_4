﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderData
{

   /**
 * Author: Prince Nimoh
 * Student #: 000792122
 * Date: July 16, 2018
 * CPRG200 - Lab 4.
 * Class containing fields for the order class
 * */
    public class Order
    {
        public int OrderID { get; set; }

        public string CustomerID { get; set; }

        public DateTime? OrderDate { get; set; }

        public DateTime? RequiredDate { get; set; }

        public DateTime? ShippedDate { get; set; }

        /// <summary>
        /// Returns the datails of this order
        /// throws an SqlException
        /// </summary>
        public List<OrderDetail> Details
        {
            get
            {
                List<OrderDetail> instanceDetails = null;

                try
                {
                    instanceDetails = OrderDetailDB.GetDetails(this.OrderID);
                }
                catch (SqlException ex)
                {
                    throw ex;
                }

                return instanceDetails;
            }
        }

        /// <summary>
        /// Property  that calculates and returns the order total
        /// Throws an SQL exception
        /// </summary>
        public decimal OrderTotal
        {
            get
            {
                decimal total = 0;
                List<OrderDetail> instanceDetails = null;

                try
                {
                    instanceDetails = Details;
                }
                catch (SqlException ex)
                {
                    throw ex;
                }

                if (instanceDetails != null)
                {
                    
                    foreach (OrderDetail d in instanceDetails)
                    {
                        //UnitPrice * (1 – Discount) * Quantity
                        total += d.UnitPrice * (1m - (decimal)d.Discount) * d.Quantity;
                    }
                }

                return total;
            }
        }

        /// <summary>
        /// Copies an order object
        /// </summary>
        /// <returns> returns a copy of this order</returns>
        public Order GetCopy()
        {
            Order copy = new Order();
            //Set properties
            copy.OrderID = OrderID;
            copy.CustomerID = CustomerID;
            copy.OrderDate = OrderDate;
            copy.RequiredDate = RequiredDate;
            copy.ShippedDate = ShippedDate;

            return copy;
        }
    }
}
