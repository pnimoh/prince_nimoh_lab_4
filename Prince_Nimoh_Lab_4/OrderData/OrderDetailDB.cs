﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderData
{
    /**
 * Author: Prince Nimoh
 * Student #: 000792122
 * Date: July 16, 2018
 * CPRG200 - Lab 4.
 * DB class for reading orders from the database
 * */
    public class OrderDetailDB
    {
        public static List<OrderDetail> GetDetails(int orderID)
        {
            List<OrderDetail> orders = new List<OrderDetail>();
            OrderDetail details = null;
            SqlConnection con = NorthwindDB.GetSqlConnection();


            string selectStatement = "SELECT * "
                                      + "From [Order Details] " +
                                      "WHERE OrderID = @OrderID " +
                                      "ORDER BY ProductID";

            SqlCommand cmd = new SqlCommand(selectStatement, con);
            cmd.Parameters.AddWithValue("@OrderID", orderID);
           
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read()) //While there are orderdetails
                {
                    details = new OrderDetail();

                    details.OrderID = Convert.ToInt32(reader["OrderID"]);
                    details.ProductID = Convert.ToInt32(reader["ProductID"]);
                    details.UnitPrice = Convert.ToDecimal(reader["UnitPrice"]);
                    details.Quantity = Convert.ToInt32(reader["Quantity"]);
                    details.Discount = Convert.ToDouble(reader["Discount"]);

                    orders.Add(details);

                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();

            }
            return orders;
        }
    }
}
