﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderData
{
   /**
 * Author: Prince Nimoh
 * Student #: 000792122
 * Date: July 16, 2018
 * CPRG200 - Lab 4.
 * DB class for reading orders from the database
 * */
    public class OrderDB
    {
        /// <summary>
        /// DB method for getting all the orders from a database
        /// </summary>
        /// <returns>List of orders from the orders table</returns>
        public static List<Order> GetAllOrders()
        {
            List<Order> orders = new List<Order>();
            Order myOrder = null;
            SqlConnection con = NorthwindDB.GetSqlConnection();

           
            string selectStatement = "SELECT OrderID, CustomerID, OrderDate, RequiredDate, ShippedDate "
                                      + "From Orders " +
                                      "ORDER BY OrderID";

            SqlCommand cmd = new SqlCommand(selectStatement, con);

            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read()) //While there are orders
                {
                    myOrder = new Order();

                    myOrder.OrderID = Convert.ToInt32(reader["OrderID"]);
                    myOrder.CustomerID = reader["CustomerID"].ToString();

                    //Reference to stackoverflow source for getting null types out of a DB
                    //https://stackoverflow.com/questions/9503698/how-to-get-nullable-datetime-out-of-the-database
                    myOrder.OrderDate = (reader["OrderDate"] == System.DBNull.Value) ? (DateTime?)null : 
                                                                                        Convert.ToDateTime(reader["OrderDate"]);
                    myOrder.RequiredDate = (reader["RequiredDate"] == System.DBNull.Value) ? (DateTime?)null :
                                                                                        Convert.ToDateTime(reader["RequiredDate"]);
                    myOrder.ShippedDate = (reader["ShippedDate"] == System.DBNull.Value) ? (DateTime?)null :
                                                                                        Convert.ToDateTime(reader["ShippedDate"]);
                    orders.Add(myOrder);

                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();

            }
            return orders;
        }


        /// <summary>
        /// Updates the shipdate of an existing order record
        /// </summary>
        /// <param name="oldOrder">data before update</param>
        /// <param name="newOrder">new data for the update</param>
        /// <returns>indicator of success</returns>
        public static bool UpdateOrderShipDate(Order oldOrder, Order newOrder)
        {
            SqlConnection con = NorthwindDB.GetSqlConnection();
            //OrderID, CustomerID, OrderDate, OrderDate, RequiredDate, and ShippedDate
            //string updateStatement = "UPDATE Orders " +
            //                          "SET ShippedDate = " + "CAST('" 
            //                          + newOrder.ShippedDate 
            //                           + "' AS DATETIME) " +
            //                          "WHERE OrderID = '" + oldOrder.OrderID.ToString() + "'"
            //                          + " AND CustomerID = '" + oldOrder.CustomerID.ToString() + "'"
            //                          + " And OrderDate = "  + "CAST('" + oldOrder.OrderDate + "' AS DATETIME) " 
            //                          + " And RequiredDate = " +"CAST('" + oldOrder.RequiredDate + "' AS DATETIME) " 
            //                          + " And ShippedDate = " +"CAST('" + oldOrder.ShippedDate + "' AS DATETIME) ";

            //string updateStatement = "UPDATE Orders " +
            //                          "SET ShippedDate = @NewShippedDate " +
            //                          "WHERE OrderID = @OldOrderID; ";

            string updateStatement = "UPDATE Orders " +
                                      "SET ShippedDate = @NewShippedDate " +
                                      "WHERE OrderID = @OldOrderID "
                                      + " AND CustomerID = @OldCustomerID"
                                        + " And OrderDate = @OldOrderDate"
                                        + " And RequiredDate = @OldRequiredDate"
                                         + " And (ShippedDate = @OldShippedDate OR @OldShippedDate IS NULL);";
                                        


            SqlCommand cmd = new SqlCommand(updateStatement, con);

            //Add the command parameters to the parameters list
            cmd.Parameters.Add("@NewShippedDate", SqlDbType.DateTime);
            cmd.Parameters.Add("@OldOrderID", SqlDbType.Int);
            cmd.Parameters.Add("@OldCustomerID", SqlDbType.NChar);
            cmd.Parameters.Add("@OldOrderDate", SqlDbType.DateTime);
            cmd.Parameters.Add("@OldRequiredDate", SqlDbType.DateTime);
            cmd.Parameters.Add("@OldShippedDate", SqlDbType.DateTime);


            //Set the values for the command parameters
            cmd.Parameters["@OldOrderID"].Value = oldOrder.OrderID;
            cmd.Parameters["@OldCustomerID"].Value = oldOrder.CustomerID;



            //Handle the null cases for ship date
            if (newOrder.ShippedDate == (DateTime?)null)
            { cmd.Parameters["@NewShippedDate"].Value = System.DBNull.Value; }
            else
            { cmd.Parameters["@NewShippedDate"].Value = newOrder.ShippedDate; }

            //Handle the null cases for order date
            if (oldOrder.OrderDate == (DateTime?)null)
            { cmd.Parameters["@OldOrderDate"].Value = System.DBNull.Value; }
            else
            { cmd.Parameters["@OldOrderDate"].Value = oldOrder.OrderDate; }

            //Handle the null cases for required date
            if (oldOrder.RequiredDate == (DateTime?)null)
            { cmd.Parameters["@OldRequiredDate"].Value = System.DBNull.Value; }
            else
            { cmd.Parameters["@OldRequiredDate"].Value = oldOrder.RequiredDate; }

            //Handle the null cases for old ship date
            if (oldOrder.ShippedDate == (DateTime?)null)
            { cmd.Parameters["@OldShippedDate"].Value = System.DBNull.Value; }
            else
            { cmd.Parameters["@OldShippedDate"].Value = oldOrder.ShippedDate; }

            try
            {
                con.Open();
                int count = cmd.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }
    }
}
