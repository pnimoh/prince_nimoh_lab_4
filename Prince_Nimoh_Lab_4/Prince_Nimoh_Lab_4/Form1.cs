﻿using OrderData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prince_Nimoh_Lab_4
{
    /**
 * Author: Prince Nimoh
 * Student #: 000792122
 * Date: July 16, 2018
 * CPRG200 - Lab 4.
 * DB class for reading orders from the database
 * */
    public partial class Form1 : Form
    {
        private List<Order> orders; //Orders to be displayed on the form
        private List<Order> originalOrders; //The copy we keep so we can track changes
        //private Order oldOrder; //Order with old shipped date
        //private Order newOrder; //Order with new shipped date
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //customers = CustomerDB.GetAllCustomer();
                //customerDataGridView.DataSource = customers; //bind the grid view to customers list
                orders = OrderDB.GetAllOrders();

                //Make of copy of the list
                originalOrders = new List<Order>();

                foreach (Order o in orders)
                {
                    originalOrders.Add(o.GetCopy());
                }


                orderBindingSource.DataSource = orders;
                orderIDComboBox.DataSource = orders; // load the combobox
                orderIDComboBox.SelectedIndex = 0; //Set the selected item to the first index
                orderIDComboBox.DisplayMember = "OrderID";
                updateDateTimeCheckBoxes();
                btnSave.Enabled = false;
                btnCancel.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while loading customers data: " + ex.Message,
                                ex.GetType().ToString());
            }
        }

        private void orderIDComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //orderIDComboBox.
            orderBindingSource.Position = orderIDComboBox.SelectedIndex;
            CancelSave(); //if there was an uncommited change cancel it
            updateDateTimeCheckBoxes();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Order newOrder = orders[orderBindingSource.Position].GetCopy();
            newOrder.ShippedDate = (shippedDateDateTimePicker.Checked) ? shippedDateDateTimePicker.Value : (DateTime?)null;
            DateTime? selectDate = (shippedDateDateTimePicker.Checked) ? shippedDateDateTimePicker.Value : (DateTime?)null;

            if (selectDate < orders[orderBindingSource.Position].OrderDate ||
                selectDate > orders[orderBindingSource.Position].RequiredDate)
               // || selectDate == (DateTime?)null)
            {
                CancelSave();
                MessageBox.Show("Invalid order date! " +
                    "Shipped Date cannot be earlier than Order Date and no later than the Required Date, " +
                    "or use check box to select null.", "Invalid Date Format", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                PerformSaveOperation(newOrder);
            }
            

        }

        private void PerformSaveOperation(Order newOrder)
        {
            bool result = false; //
            //Try to update the database
            try
            {
                result = OrderDB.UpdateOrderShipDate(originalOrders[orderBindingSource.Position], newOrder);

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Something went wrong with the database", ex.GetType().ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (!result)
                {

                    CancelSave();
                    MessageBox.Show("Not saved to Database!", "Save status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Saved to Database!", "Save status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //Save worked so update the binding source
                    orders[orderBindingSource.Position] = newOrder;
                    //orders[orderBindingSource.Position] = originalOrders[orderBindingSource.Position].GetCopy();
                    originalOrders[orderBindingSource.Position] = orders[orderBindingSource.Position].GetCopy();
                    orderBindingSource.ResetCurrentItem();
                    updateDateTimeCheckBoxes();
                    btnSave.Enabled = false;
                    btnCancel.Enabled = false;

                }
            }
        }

        //Set up UI controls when the shippedDateTimePicker value changes
        private void shippedDateDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            //oldOrder = orders[orderBindingSource.Position].GetCopy(); //make a copy in preparation for save operation
        }

        //Set up UI controls when the shippedDateTimePicker is gets focus
        private void shippedDateDateTimePicker_Enter(object sender, EventArgs e)
        {
            //btnSave.Enabled = true;
            updateDateTimeCheckBoxes();
            //shippedDateDateTimePicker.MaxDate = (DateTime)orders[orderBindingSource.Position].RequiredDate;
            //shippedDateDateTimePicker.MinDate = (DateTime)orders[orderBindingSource.Position].OrderDate;

        }

        //Updates the checkbox on the datetime pickers based on null values
        private void updateDateTimeCheckBoxes()
        {
            //This code snippet checks the check boxes on  and of based on null value
            orderDateDateTimePicker.Checked = (orders[orderIDComboBox.SelectedIndex].OrderDate == null)
                 ? false : true;

            //Toggle the required date check box on and off based null value
            requiredDateDateTimePicker.Checked = (orders[orderIDComboBox.SelectedIndex].RequiredDate == null)
                ? false : true;

            //Toggle the shipped date check box on and off based null value
            shippedDateDateTimePicker.Checked = (orders[orderIDComboBox.SelectedIndex].ShippedDate == null)
                ? false : true;
        }

        //Responses to the user action to Cancel unsave changes for current record
        private void btnCancel_Click(object sender, EventArgs e)
        {
            CancelSave();
        }

        //Cancels the current changes
        private void CancelSave()
        {
            //Roll back the datasource as the change was cancelled
            orders[orderBindingSource.Position] = originalOrders[orderBindingSource.Position].GetCopy();
            
            orderBindingSource.ResetCurrentItem();
            updateDateTimeCheckBoxes();
            btnSave.Enabled = false;
            btnCancel.Enabled = false;
        }

    }
}
